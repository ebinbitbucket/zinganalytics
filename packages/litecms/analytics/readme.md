Lavalite package that provides analytics management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `litecms/analytics`.

    "litecms/analytics": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Litecms\Analytics\Providers\AnalyticsServiceProvider::class,

And also add it to alias

    'Analytics'  => Litecms\Analytics\Facades\Analytics::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Litecms\\AnalyticsTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="view"


### Web Urls

**Admin**

    http://path-to-route-folder/admin/analytics/{modulename}

**User**

    http://path-to-route-folder/user/analytics/{modulename}

**Public**

    http://path-to-route-folder/analytics


### API endpoints

**List**

    http://path-to-route-folder/api/user/analytics/{modulename}
    METHOD: GET

**Create**

    http://path-to-route-folder/api/user/analytics/{modulename}
    METHOD: POST

**Edit**

    http://path-to-route-folder/api/user/analytics/{modulename}/{id}
    METHOD: PUT

**Delete**

    http://path-to-route-folder/api/user/analytics/{modulename}/{id}
    METHOD: DELETE

**Public List**

    http://path-to-route-folder/api/analytics/{modulename}
    METHOD: GET

**Public Single**

    http://path-to-route-folder/api/analytics/{modulename}/{slug}
    METHOD: GET