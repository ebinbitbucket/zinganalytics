<?php

// API routes  for transaction_log
Route::prefix('{guard}/analytics')->group(function () {
    Route::get('transaction_log/form/{element}', 'TransactionLogAPIController@form');
    Route::resource('transaction_log', 'TransactionLogAPIController');
});

// Public routes for transaction_log
Route::get('analytics/', 'TransactionLogPublicController@index');
Route::get('analytics/{slug?}', 'TransactionLogPublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where'  => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for analytics
            Route::prefix('{guard}/analytics')->group(function () {
                Route::get('transaction_log/form/{element}', 'TransactionLogAPIController@form');
                Route::apiResource('transaction_log', 'TransactionLogAPIController');
            });
            // Public routes for analytics
            Route::get('analytics/TransactionLog', 'TransactionLogPublicController@getTransactionLog');
        }
    );
}

