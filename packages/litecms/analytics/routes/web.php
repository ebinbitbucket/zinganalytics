<?php

// web routes  for transaction_log
Route::prefix('{guard}/analytics')->group(function () {
    Route::resource('transaction_log', 'TransactionLogResourceController');
});

// Public routes for transaction_log
Route::get('analytics/', 'TransactionLogPublicController@index');
Route::post('analytics/save', 'TransactionLogPublicController@saveClientInformation');
Route::post('analytics/create', 'TransactionLogPublicController@createClientInformation');


// 
Route::get('analytics/{slug?}', 'TransactionLogPublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where'  => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for pages
            Route::prefix('{guard}/page')->group(function () {
                Route::apiResource('page', 'TransactionLogResourceController');
            });
            // Public routes for pages
            Route::get('analytics/', 'TransactionLogPublicController@index');
            Route::get('analytics/{slug?}', 'TransactionLogPublicController@show');
        }
    );
}

