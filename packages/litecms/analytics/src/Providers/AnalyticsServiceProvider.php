<?php

namespace Litecms\Analytics\Providers;

use Illuminate\Support\ServiceProvider;

class AnalyticsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'analytics');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'analytics');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerAnalytics();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerAnalytics()
    {
        $this->app->bind('analytics', function($app) {
            return new Analytics($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Analytics', 'Lavalite\Analytics\Facades\Analytics');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('litecms.analytics', function ($app) {
            return $this->app->make('Litecms\Analytics\Analytics');
        });

                // Bind TransactionLog to repository
        $this->app->bind(
            'Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface',
            \Litecms\Analytics\Repositories\Eloquent\TransactionLogRepository::class
        );

        $this->app->register(\Litecms\Analytics\Providers\AuthServiceProvider::class);
                $this->app->register(\Litecms\Analytics\Providers\EventServiceProvider::class);
        
        $this->app->register(\Litecms\Analytics\Providers\RouteServiceProvider::class);
                // $this->app->register(\Litecms\Analytics\Providers\WorkflowServiceProvider::class);
            }

    /**
     * Merges user's and analytics's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'litecms.analytics'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeAnalytics::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['litecms.analytics'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('litecms/analytics.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/analytics')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/analytics')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
