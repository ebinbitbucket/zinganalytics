<?php

namespace Litecms\Analytics\Providers;

use Litepie\Contracts\Workflow\Workflow as WorkflowContract;
use Litepie\Foundation\Support\Providers\WorkflowServiceProvider as ServiceProvider;

class WorkflowServiceProvider extends ServiceProvider
{
    /**
     * The validators mappings for the package.
     *
     * @var array
     */
    protected $validators = [
        
        // Bind TransactionLog workflow validator
        'Litecms\Analytics\Models\TransactionLog' => \Litecms\Analytics\Workflow\TransactionLogValidator::class,
    ];

    /**
     * The actions mappings for the package.
     *
     * @var array
     */
    protected $actions = [
        
        // Bind TransactionLog workflow actions
        'Litecms\Analytics\Models\TransactionLog' => \Litecms\Analytics\Workflow\TransactionLogAction::class,
    ];

    /**
     * The notifiers mappings for the package.
     *
     * @var array
     */
    protected $notifiers = [
       
        // Bind TransactionLog workflow notifiers
        'Litecms\Analytics\Models\TransactionLog' => \Litecms\Analytics\Workflow\TransactionLogNotifier::class,
    ];

    /**
     * Register any package workflow validation services.
     *
     * @param \Litepie\Contracts\Workflow\Workflow $workflow
     *
     * @return void
     */
    public function boot(WorkflowContract $workflow)
    {
        parent::registerValidators($workflow);
        parent::registerActions($workflow);
        parent::registerNotifiers($workflow);
    }
}
