<?php

namespace Litecms\Analytics\Policies;

use Litepie\User\Contracts\UserPolicy;
use Litecms\Analytics\Models\TransactionLog;

class TransactionLogPolicy
{

    /**
     * Determine if the given user can view the transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function view(UserPolicy $user, TransactionLog $transaction_log)
    {
        if ($user->canDo('analytics.transaction_log.view') && $user->isAdmin()) {
            return true;
        }

        return $transaction_log->user_id == user_id() && $transaction_log->user_type == user_type();
    }

    /**
     * Determine if the given user can create a transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('analytics.transaction_log.create');
    }

    /**
     * Determine if the given user can update the given transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function update(UserPolicy $user, TransactionLog $transaction_log)
    {
        if ($user->canDo('analytics.transaction_log.edit') && $user->isAdmin()) {
            return true;
        }

        return $transaction_log->user_id == user_id() && $transaction_log->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, TransactionLog $transaction_log)
    {
        return $transaction_log->user_id == user_id() && $transaction_log->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function verify(UserPolicy $user, TransactionLog $transaction_log)
    {
        if ($user->canDo('analytics.transaction_log.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given transaction_log.
     *
     * @param UserPolicy $user
     * @param TransactionLog $transaction_log
     *
     * @return bool
     */
    public function approve(UserPolicy $user, TransactionLog $transaction_log)
    {
        if ($user->canDo('analytics.transaction_log.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
