<?php

namespace Litecms\Analytics;

use User;

class Analytics
{
    /**
     * $transaction_log object.
     */
    protected $transaction_log;

    /**
     * Constructor.
     */
    public function __construct(\Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface $transaction_log)
    {
        $this->transaction_log = $transaction_log;
    }

    /**
     * Returns count of analytics.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.transaction_log.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->transaction_log->pushCriteria(new \Litepie\Litecms\Repositories\Criteria\TransactionLogUserCriteria());
        }

        $transaction_log = $this->transaction_log->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('analytics::' . $view, compact('transaction_log'))->render();
    }
}
