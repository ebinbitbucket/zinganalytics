<?php

namespace Litecms\Analytics\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Litecms\Analytics\Http\Requests\TransactionLogRequest;
use Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface;
use Litecms\Analytics\Models\TransactionLog;

/**
 * Resource controller class for transaction_log.
 */
class TransactionLogResourceController extends BaseController
{

    /**
     * Initialize transaction_log resource controller.
     *
     * @param type TransactionLogRepositoryInterface $transaction_log
     *
     * @return null
     */
    public function __construct(TransactionLogRepositoryInterface $transaction_log)
    {
        parent::__construct();
        $this->repository = $transaction_log;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Litecms\Analytics\Repositories\Criteria\TransactionLogResourceCriteria::class);
    }

    /**
     * Display a list of transaction_log.
     *
     * @return Response
     */
    public function index(TransactionLogRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Litecms\Analytics\Repositories\Presenter\TransactionLogPresenter::class)
                ->$function();
        }

        $transaction_logs = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('analytics::transaction_log.names'))
            ->view('analytics::transaction_log.index', true)
            ->data(compact('transaction_logs', 'view'))
            ->output();
    }

    /**
     * Display transaction_log.
     *
     * @param Request $request
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function show(TransactionLogRequest $request, TransactionLog $transaction_log)
    {

        if ($transaction_log->exists) {
            $view = 'analytics::transaction_log.show';
        } else {
            $view = 'analytics::transaction_log.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('analytics::transaction_log.name'))
            ->data(compact('transaction_log'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new transaction_log.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(TransactionLogRequest $request)
    {

        $transaction_log = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('analytics::transaction_log.name')) 
            ->view('analytics::transaction_log.create', true) 
            ->data(compact('transaction_log'))
            ->output();
    }

    /**
     * Create new transaction_log.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(TransactionLogRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $transaction_log                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('analytics::transaction_log.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/analytics/transaction_log'))
                ->redirect();
        }

    }

    /**
     * Show transaction_log for editing.
     *
     * @param Request $request
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function edit(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('analytics::transaction_log.name'))
            ->view('analytics::transaction_log.edit', true)
            ->data(compact('transaction_log'))
            ->output();
    }

    /**
     * Update the transaction_log.
     *
     * @param Request $request
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function update(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        try {
            $attributes = $request->all();

            $transaction_log->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('analytics::transaction_log.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the transaction_log.
     *
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function destroy(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        try {

            $transaction_log->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('analytics::transaction_log.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('analytics/transaction_log/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple transaction_log.
     *
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function delete(TransactionLogRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('analytics::transaction_log.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('analytics/transaction_log'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/analytics/transaction_log'))
                ->redirect();
        }

    }

    /**
     * Restore deleted transaction_logs.
     *
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function restore(TransactionLogRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('analytics::transaction_log.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/analytics/transaction_log'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/analytics/transaction_log/'))
                ->redirect();
        }

    }

    public function saveClientInformation(TransactionLogRequest $request)
    {
    
        dd($request->all());
    }

}
