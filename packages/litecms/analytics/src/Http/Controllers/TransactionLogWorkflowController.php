<?php

namespace Litecms\Analytics\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Litecms\Analytics\Http\Requests\TransactionLogRequest;
use Litecms\Analytics\Models\TransactionLog;

/**
 * Admin web controller class.
 */
class TransactionLogWorkflowController extends BaseController
{

    /**
     * Workflow controller function for transaction_log.
     *
     * @param Model   $transaction_log
     * @param step    next step for the workflow.
     *
     * @return Response
     */

    public function putWorkflow(TransactionLogRequest $request, TransactionLog $transaction_log, $step)
    {

        try {

            $transaction_log->updateWorkflow($step);

            return response()->json([
                'message'  => trans('messages.success.changed', ['Module' => trans('analytics::transaction_log.name'), 'status' => trans("app.{$step}")]),
                'code'     => 204,
                'redirect' => trans_url('/admin/transaction_log/transaction_log/' . $transaction_log->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/transaction_log/transaction_log/' . $transaction_log->getRouteKey()),
            ], 400);

        }

    }

    /**
     * Workflow controller function for transaction_log.
     *
     * @param Model   $transaction_log
     * @param step    next step for the workflow.
     * @param user    encrypted user id.
     *
     * @return Response
     */

    public function getWorkflow(TransactionLog $transaction_log, $step, $user)
    {
        try {
            $user_id = decrypt($user);

            Auth::onceUsingId($user_id);

            $transaction_log->updateWorkflow($step);

            $data = [
                'message' => trans('messages.success.changed', ['Module' => trans('analytics::transaction_log.name'), 'status' => trans("app.{$step}")]),
                'status'  => 'success',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('analytics::admin.transaction_log.message', $data)->render();

        } catch (ValidationException $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b> <br /><br />' . implode('<br />', $e->validator->errors()->all()),
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('analytics::admin.transaction_log.message', $data)->render();

        } catch (Exception $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b>',
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('analytics::admin.transaction_log.message', $data)->render();

        }

    }
}
