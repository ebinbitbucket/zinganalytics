<?php

namespace Litecms\Analytics\Http\Controllers;

use App\Http\Controllers\APIController as BaseController;
use Litecms\Analytics\Http\Requests\TransactionLogRequest;
use Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface;
use Litecms\Analytics\Models\TransactionLog;
use Litecms\Analytics\Forms\TransactionLog as Form;

/**
 * APIController  class for transaction_log.
 */
class TransactionLogAPIController extends BaseController
{

    /**
     * Initialize transaction_log resource controller.
     *
     * @param type TransactionLogRepositoryInterface $transaction_log
     *
     * @return null
     */
    public function __construct(TransactionLogRepositoryInterface $transaction_log)
    {
        parent::__construct();
        $this->repository = $transaction_log;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Litecms\Analytics\Repositories\Criteria\TransactionLogResourceCriteria::class);
    }

    /**
     * Display a list of transaction_log.
     *
     * @return Response
     */
    public function index(TransactionLogRequest $request)
    {
        return $this->repository
            ->setPresenter(\Litecms\Analytics\Repositories\Presenter\TransactionLogPresenter::class)
            ->paginate();
    }

    /**
     * Display transaction_log.
     *
     * @param Request $request
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function show(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        return $transaction_log->setPresenter(\Litecms\Analytics\Repositories\Presenter\TransactionLogListPresenter::class);
        ;
    }

    /**
     * Create new transaction_log.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(TransactionLogRequest $request)
    {
        try {
            $data              = $request->all();
            $data['user_id']   = user_id();
            $data['user_type'] = user_type();
            $data              = $this->repository->create($data);
            $message           = trans('messages.success.created', ['Module' => trans('analytics::transaction_log.name')]);
            $code              = 204;
            $status            = 'success';
            $url               = guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey());
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('analytics/transaction_log');
        }
        return compact('data', 'message', 'code', 'status', 'url');
    }

    /**
     * Update the transaction_log.
     *
     * @param Request $request
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function update(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        try {
            $data = $request->all();

            $transaction_log->update($data);
            $message = trans('messages.success.updated', ['Module' => trans('analytics::transaction_log.name')]);
            $code    = 204;
            $status  = 'success';
            $url     = guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey());
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey());
        }
        return compact('data', 'message', 'code', 'status', 'url');
    }

    /**
     * Remove the transaction_log.
     *
     * @param Model   $transaction_log
     *
     * @return Response
     */
    public function destroy(TransactionLogRequest $request, TransactionLog $transaction_log)
    {
        try {
            $transaction_log->delete();
            $message = trans('messages.success.deleted', ['Module' => trans('analytics::transaction_log.name')]);
            $code    = 202;
            $status  = 'success';
            $url     = guard_url('analytics/transaction_log/0');
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('analytics/transaction_log/' . $transaction_log->getRouteKey());
        }
        return compact('message', 'code', 'status', 'url');
    }

    /**
     * Return the form elements as json.
     *
     * @param String   $element
     *
     * @return json
     */
    public function form($element = 'fields')
    {
        $form = new Form();
        return $form->form($element, true);
    }

}
