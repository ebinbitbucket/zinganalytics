<?php

namespace Litecms\Analytics\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface;
// use Litecms\Analytics\Http\Requests\TransactionLogRequest;
use illuminate\Http\Request;
use Litecms\Analytics\Models\TransactionLog;
use DB;
use Carbon\Carbon;
use DateTime;
class TransactionLogPublicController extends BaseController
{
    // use TransactionLogWorkflow;

    /**
     * Constructor.
     *
     * @param type \Litecms\TransactionLog\Interfaces\TransactionLogRepositoryInterface $transaction_log
     *
     * @return type
     */
    public function __construct(TransactionLogRepositoryInterface $transaction_log)
    {
        $this->repository = $transaction_log;
        parent::__construct();
    }

    /**
     * Show transaction_log's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $transaction_logs = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('analytics::transaction_log.names'))
            ->view('analytics::public.transaction_log.index')
            ->data(compact('transaction_logs'))
            ->output();
    }


    /**
     * Show transaction_log.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $transaction_log = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($transaction_log->name . trans('analytics::transaction_log.name'))
            ->view('analytics::public.transaction_log.show')
            ->data(compact('transaction_log'))
            ->output();
    }
    public function saveClientInformation(Request $request)
    {
        $attributes = json_decode($request->getContent(),true);
        $current_date_time = Carbon::now()->toDateTimeString();

        $dt = new DateTime;
            $dt->setTime(0, 0);
            $dateTime = $dt->format('Y-m-d H:i:s');
            if($attributes['ip_address'] == '::1'){
                $attributes['ip_address'] = '127.0.0.1';
            }
            if($attributes['user_id'] == ""){
                $attributes['user_id'] = null;
            }
            if(isset($attributes['addon'])){
                if($attributes['addon'] == ""){
                    $attributes['addon'] = null;
                }
            }
            if(isset($attributes['json_data'])){
                if($attributes['json_data'] == ""){
                    $attributes['json_data'] = null;
                }
            }




            // $log = DB::table('transaction_logs')->insertGetId([
            //     'user_id'=>@$attributes['user_id'],
            //     'type'=>@$attributes['type'],
            //     'ip_address'=>@$attributes['ip_address'],
            //     'date'=>$dateTime,
            //     'restaurant_id'=>@$attributes['restaurant_id'],
            //     'url'=>@$attributes['url'],
            //     'server_variables'=>@$attributes['server_variables'],
            //     'HTTP_REFERER'=>@$attributes['HTTP_REFERER'],
            //     'source'=>@$attributes['source'],
            //     'added_item'=>@$attributes['addon'],
            //     'cart_id'=>@$attributes['cart_id'],
            //     'json_data'=>@$attributes['json_data'],
            //     'total_amount'=>@$attributes['total_amount'],

            // ]);

            $log = TransactionLog::updateOrCreate(
                [
                    'date'=>$dateTime,
                    'type'=>$attributes['type'],
                    'ip_address'=>$attributes['ip_address'],
                    'restaurant_id'=>$attributes['restaurant_id'],
                    
                ],[
                    'user_id'=>@$attributes['user_id'],
                    'type'=>@$attributes['type'],
                    'ip_address'=>@$attributes['ip_address'],
                    'date'=>$dateTime,
                    'restaurant_id'=>@$attributes['restaurant_id'],
                    'url'=>@$attributes['url'],
                    'server_variables'=>@$attributes['server_variables'],
                    'HTTP_REFERER'=>@$attributes['HTTP_REFERER'],
                    'source'=>@$attributes['source'],
                    'added_item'=>@$attributes['addon'],
                    'cart_id'=>@$attributes['cart_id'],
                    'json_data'=>@$attributes['json_data'],
                    'total_amount'=>@$attributes['total_amount'],
                    
                ]);

            return $log;
        // }
        // dd($log);
        if($log == true){
            return response(['status'=>'success']);
        }else{
            return response(['status'=>'false']);
        }

    }

    public function createClientInformation(Request $request)
    {
        $attributes = json_decode($request->getContent(),true);
        $current_date_time = Carbon::now()->toDateTimeString();

        $dt = new DateTime;
            $dt->setTime(0, 0);
            $dateTime = $dt->format('Y-m-d H:i:s');
            if($attributes['ip_address'] == '::1'){
                $attributes['ip_address'] = '127.0.0.1';
            }
            if($attributes['user_id'] == ""){
                $attributes['user_id'] = null;
            }
            if(isset($attributes['addon'])){
                if($attributes['addon'] == ""){
                    $attributes['addon'] = null;
                }
            }
            if(isset($attributes['json_data'])){
                if($attributes['json_data'] == ""){
                    $attributes['json_data'] = null;
                }
            }


            $log = DB::table('transaction_logs')->insertGetId([
                'user_id'=>@$attributes['user_id'],
                'type'=>@$attributes['type'],
                'ip_address'=>@$attributes['ip_address'],
                'date'=>$dateTime,
                'restaurant_id'=>@$attributes['restaurant_id'],
                'url'=>@$attributes['url'],
                'server_variables'=>@$attributes['server_variables'],
                'HTTP_REFERER'=>@$attributes['HTTP_REFERER'],
                'source'=>@$attributes['source'],
                'added_item'=>@$attributes['addon'],
                'cart_id'=>@$attributes['cart_id'],
                'json_data'=>@$attributes['json_data'],
                'total_amount'=>@$attributes['total_amount'],

            ]);
            return $log;

    }

    
    

}
