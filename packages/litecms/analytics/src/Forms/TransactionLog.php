<?php

namespace Litecms\Analytics\Forms;

class TransactionLog
{
    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $form;

    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $element;

    /**
     * Initialize the form.
     *
     * @return null
     */
    public function __construct()
    {
        $this->setForm();
    }

    /**
     * Return form elements.
     *
     * @return array.
     */
    public function form($element = 'fields', $grouped = true)
    {
        $item = collect($this->form->get($element));
        if ($element == 'fields' && $grouped == true) {
            return $item->groupBy(['group', 'section']);
        }
        return $item;

    }

    /**
     * Sets the form and form elements.
     * @return null.
     */
    public function setForm()
    {
        $this->form = collect([
            'form' => [
                'store' => [],
                'update' => [],
            ],
            'groups' => [
                'main' => 'Main',
            ],
            'fields' => [
                'user_id' => [
                    "type" => 'numeric',
                    "label" => trans('analytics::transaction_log.label.user_id'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.user_id'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'ip_address' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.ip_address'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.ip_address'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'date' => [
                    "type" => 'date_time_picker',
                    "label" => trans('analytics::transaction_log.label.date'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.date'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'type' => [
                    "type" => 'radios',
                    "label" => trans('analytics::transaction_log.label.type'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.type'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'avs_code' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.avs_code'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.avs_code'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'total_amount' => [
                    "type" => 'decimal',
                    "label" => trans('analytics::transaction_log.label.total_amount'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.total_amount'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'card' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.card'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.card'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'json_data' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.json_data'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.json_data'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'cart_id' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.cart_id'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.cart_id'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'added_item' => [
                    "type" => 'numeric',
                    "label" => trans('analytics::transaction_log.label.added_item'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.added_item'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'restaurant_id' => [
                    "type" => 'numeric',
                    "label" => trans('analytics::transaction_log.label.restaurant_id'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.restaurant_id'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'url' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.url'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.url'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'source' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.source'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.source'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'server_variables' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.server_variables'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.server_variables'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'HTTP_REFERER' => [
                    "type" => 'text',
                    "label" => trans('analytics::transaction_log.label.HTTP_REFERER'),
                    "placeholder" => trans('analytics::transaction_log.placeholder.HTTP_REFERER'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
            ]
        );

    }
}
