<?php

namespace Litecms\Analytics\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class TransactionLogPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TransactionLogTransformer();
    }
}