<?php

namespace Litecms\Analytics\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class TransactionLogTransformer extends TransformerAbstract
{
    public function transform(\Litecms\Analytics\Models\TransactionLog $transaction_log)
    {
        return [
            'id'                => $transaction_log->getRouteKey(),
            'key'               => [
                'public'    => $transaction_log->getPublicKey(),
                'route'     => $transaction_log->getRouteKey(),
            ], 
            'user_id'           => $transaction_log->user_id,
            'ip_address'        => $transaction_log->ip_address,
            'date'              => $transaction_log->date,
            'type'              => $transaction_log->type,
            'avs_code'          => $transaction_log->avs_code,
            'total_amount'      => $transaction_log->total_amount,
            'card'              => $transaction_log->card,
            'json_data'         => $transaction_log->json_data,
            'cart_id'           => $transaction_log->cart_id,
            'added_item'        => $transaction_log->added_item,
            'restaurant_id'     => $transaction_log->restaurant_id,
            'url'               => $transaction_log->url,
            'source'            => $transaction_log->source,
            'server_variables'  => $transaction_log->server_variables,
            'HTTP_REFERER'      => $transaction_log->HTTP_REFERER,
            'deleted_at'        => $transaction_log->deleted_at,
            'url'               => [
                'public'    => trans_url('analytics/'.$transaction_log->getPublicKey()),
                'user'      => guard_url('analytics/transaction_log/'.$transaction_log->getRouteKey()),
            ], 
            'status'            => trans('app.'.$transaction_log->status),
            'created_at'        => format_date($transaction_log->created_at),
            'updated_at'        => format_date($transaction_log->updated_at),
        ];
    }
}