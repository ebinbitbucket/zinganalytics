<?php

namespace Litecms\Analytics\Repositories\Eloquent;

use Litecms\Analytics\Interfaces\TransactionLogRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class TransactionLogRepository extends BaseRepository implements TransactionLogRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('litecms.analytics.transaction_log.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('litecms.analytics.transaction_log.model.model');
    }
}
