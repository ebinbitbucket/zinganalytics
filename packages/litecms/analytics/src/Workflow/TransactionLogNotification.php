<?php

namespace Litecms\Analytics\Workflow;

use Litecms\Analytics\Models\TransactionLog;
use Litecms\Analytics\Notifications\TransactionLogWorkflow as TransactionLogNotifyer;
use Notification;

class TransactionLogNotification
{

    /**
     * Send the notification to the users after complete.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function complete(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'complete'));;
    }

    /**
     * Send the notification to the users after verify.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function verify(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'verify'));;
    }

    /**
     * Send the notification to the users after approve.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function approve(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'approve'));;

    }

    /**
     * Send the notification to the users after publish.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function publish(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'publish'));;
    }

    /**
     * Send the notification to the users after archive.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function archive(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'archive'));;

    }

    /**
     * Send the notification to the users after unpublish.
     *
     * @param TransactionLog $transaction_log
     *
     * @return void
     */
    public function unpublish(TransactionLog $transaction_log)
    {
        return Notification::send($transaction_log->user, new TransactionLogNotifyer($transaction_log, 'unpublish'));;

    }
}
