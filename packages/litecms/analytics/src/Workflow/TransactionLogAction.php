<?php

namespace Litecms\Analytics\Workflow;

use Exception;
use Litepie\Workflow\Exceptions\WorkflowActionNotPerformedException;

use Litecms\Analytics\Models\TransactionLog;

class TransactionLogAction
{
    /**
     * Perform the complete action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */
    public function complete(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'complete';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the verify action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */public function verify(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'verify';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the approve action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */public function approve(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'approve';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the publish action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */public function publish(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'publish';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the archive action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */
    public function archive(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'archive';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the unpublish action.
     *
     * @param TransactionLog $transaction_log
     *
     * @return TransactionLog
     */
    public function unpublish(TransactionLog $transaction_log)
    {
        try {
            $transaction_log->status = 'unpublish';
            return $transaction_log->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }
}
