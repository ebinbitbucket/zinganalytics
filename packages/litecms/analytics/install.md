# Installation

The instructions below will help you to properly install the generated package to the lavalite project.

## Location

Extract the package contents to the folder 

`/packages/litecms/analytics/`

## Composer

Add the below entries in the `composer.json` file's autoload section and run the command `composer dump-autoload` in terminal.

```json

...
     "autoload": {
         ...

        "classmap": [
            ...
            
            "packages/litecms/analytics/database/seeds",
            
            ...
        ],
        "psr-4": {
            ...
            
            "Litecms\\Analytics\\": "packages/litecms/analytics/src",
            
            ...
        }
    },
...

```

## Config

Add the entries in service provider in `config/app.php`

```php

...
    'providers'       => [
        ...
        
        Litecms\Analytics\Providers\AnalyticsServiceProvider::class,
        
        ...
    ],

    ...

    'alias'             => [
        ...
        
        'Analytics'  => Litecms\Analytics\Facades\Analytics::class,
        
        ...
    ]
...

```

## Migrate

After service provider is set run the commapnd to migrate and seed the database.


    php artisan migrate
    php artisan db:seed --class=Litecms\\AnalyticsTableSeeder

## Publishing


**Publishing configuration**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Litecms\Analytics\Providers\AnalyticsServiceProvider" --tag="view"


## URLs and APIs


### Web Urls

**Admin**

    http://path-to-route-folder/admin/analytics/{modulename}

**User**

    http://path-to-route-folder/user/analytics/{modulename}

**Public**

    http://path-to-route-folder/analytics


### API endpoints

**List**
 
    http://path-to-route-folder/api/user/analytics/{modulename}
    METHOD: GET

**Create**

    http://path-to-route-folder/api/user/analytics/{modulename}
    METHOD: POST

**Edit**

    http://path-to-route-folder/api/user/analytics/{modulename}/{id}
    METHOD: PUT

**Delete**

    http://path-to-route-folder/api/user/analytics/{modulename}/{id}
    METHOD: DELETE

**Public List**

    http://path-to-route-folder/api/analytics/{modulename}
    METHOD: GET

**Public Single**

    http://path-to-route-folder/api/analytics/{modulename}/{slug}
    METHOD: GET