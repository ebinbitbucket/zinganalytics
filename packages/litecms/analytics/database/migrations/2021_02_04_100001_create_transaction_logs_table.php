<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionLogsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: transaction_logs
         */
        Schema::create('transaction_logs', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('ip_address', 255)->nullable();
            $table->timestamp('date')->nullable();
            $table->enum('type', ['card','cart','restaurant','eateryview','login','checkout','payment'])->nullable();
            $table->string('avs_code', 255)->nullable();
            $table->decimal('total_amount', 10,2)->nullable();
            $table->string('card', 255)->nullable();
            $table->text('json_data')->nullable();
            $table->string('cart_id', 255)->nullable();
            $table->integer('added_item')->nullable();
            $table->integer('restaurant_id')->nullable();
            $table->string('url', 255)->nullable();
            $table->string('source', 255)->nullable();
            $table->text('server_variables')->nullable();
            $table->text('HTTP_REFERER')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('transaction_logs');
    }
}
