<?php

namespace Litecms\Analytics;

use DB;
use Illuminate\Database\Seeder;

class TransactionLogTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('transaction_logs')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'analytics.transaction_log.view',
                'name'      => 'View TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.create',
                'name'      => 'Create TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.edit',
                'name'      => 'Update TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.delete',
                'name'      => 'Delete TransactionLog',
            ],
            
            // Customize this permissions if needed.
            [
                'slug'      => 'analytics.transaction_log.verify',
                'name'      => 'Verify TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.approve',
                'name'      => 'Approve TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.publish',
                'name'      => 'Publish TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.unpublish',
                'name'      => 'Unpublish TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.cancel',
                'name'      => 'Cancel TransactionLog',
            ],
            [
                'slug'      => 'analytics.transaction_log.archive',
                'name'      => 'Archive TransactionLog',
            ],
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/analytics/transaction_log',
                'name'        => 'TransactionLog',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/analytics/transaction_log',
                'name'        => 'TransactionLog',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'transaction_log',
                'name'        => 'TransactionLog',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Analytics',
                'module'    => 'TransactionLog',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'analytics.transaction_log.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
