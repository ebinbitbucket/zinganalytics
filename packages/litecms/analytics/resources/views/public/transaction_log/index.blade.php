            @include('analytics::public.transaction_log.partial.header')

            <section class="grid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('analytics::public.transaction_log.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="main-area parent-border list-item">
                                @foreach($transaction_logs as $transaction_log)
                                <div class="item border">
                                    <div class="feature">
                                        <a href="{{trans_url('transaction_logs')}}/{{@$transaction_log['slug']}}">
                                            <img src="{{url($transaction_log->defaultImage('images'))}}" class="img-responsive center-block" alt="">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h4><a href="{{trans_url('transaction_log')}}/{{$transaction_log['slug']}}">{{str_limit($transaction_log['title'], 300)}}</a> 
                                        </h4>
                                        <div class="metas mt20">
                                            <div class="tag pull-left">
                                                <a href="#" class="">Seo Tips</a>
                                            </div>
                                            <div class="date-time pull-right">
                                                <span><i class="fa fa-comments"></i>{{@$transaction_log->viewcount}}</span>
                                                <span><i class="fa fa-calendar"></i>{{format_date($transaction_log['posted_on'])}}</span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="author">
                                            <div class="avatar pull-left">
                                                {{@$transaction_log->user->badge}}
                                            </div>
                                            <div class="actions">
                                                

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @endforeach
                            </div>
                            <div class="pagination text-center">
                            {{ $transaction_logs->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section> 