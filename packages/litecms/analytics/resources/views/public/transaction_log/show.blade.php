            @include('analytics::transaction_log.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('analytics::transaction_log.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($transaction_log->defaultImage('images' , 'xl'))!!}" alt="{{$transaction_log->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('analytics::transaction_log.label.id') !!}
                </label><br />
                    {!! $transaction_log['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('analytics::transaction_log.label.user_id') !!}
                </label><br />
                    {!! $transaction_log['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="ip_address">
                    {!! trans('analytics::transaction_log.label.ip_address') !!}
                </label><br />
                    {!! $transaction_log['ip_address'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date">
                    {!! trans('analytics::transaction_log.label.date') !!}
                </label><br />
                    {!! $transaction_log['date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="type">
                    {!! trans('analytics::transaction_log.label.type') !!}
                </label><br />
                    {!! $transaction_log['type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="avs_code">
                    {!! trans('analytics::transaction_log.label.avs_code') !!}
                </label><br />
                    {!! $transaction_log['avs_code'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="total_amount">
                    {!! trans('analytics::transaction_log.label.total_amount') !!}
                </label><br />
                    {!! $transaction_log['total_amount'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="card">
                    {!! trans('analytics::transaction_log.label.card') !!}
                </label><br />
                    {!! $transaction_log['card'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="json_data">
                    {!! trans('analytics::transaction_log.label.json_data') !!}
                </label><br />
                    {!! $transaction_log['json_data'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="cart_id">
                    {!! trans('analytics::transaction_log.label.cart_id') !!}
                </label><br />
                    {!! $transaction_log['cart_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="added_item">
                    {!! trans('analytics::transaction_log.label.added_item') !!}
                </label><br />
                    {!! $transaction_log['added_item'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('analytics::transaction_log.label.restaurant_id') !!}
                </label><br />
                    {!! $transaction_log['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="url">
                    {!! trans('analytics::transaction_log.label.url') !!}
                </label><br />
                    {!! $transaction_log['url'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="source">
                    {!! trans('analytics::transaction_log.label.source') !!}
                </label><br />
                    {!! $transaction_log['source'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="server_variables">
                    {!! trans('analytics::transaction_log.label.server_variables') !!}
                </label><br />
                    {!! $transaction_log['server_variables'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="HTTP_REFERER">
                    {!! trans('analytics::transaction_log.label.HTTP_REFERER') !!}
                </label><br />
                    {!! $transaction_log['HTTP_REFERER'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('analytics::transaction_log.label.created_at') !!}
                </label><br />
                    {!! $transaction_log['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('analytics::transaction_log.label.updated_at') !!}
                </label><br />
                    {!! $transaction_log['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('analytics::transaction_log.label.deleted_at') !!}
                </label><br />
                    {!! $transaction_log['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('user_id')
                       -> label(trans('analytics::transaction_log.label.user_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.user_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('ip_address')
                       -> label(trans('analytics::transaction_log.label.ip_address'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.ip_address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='date' class='control-label'>{!!trans('analytics::transaction_log.label.date')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('date')
                            -> placeholder(trans('analytics::transaction_log.placeholder.date'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('type')
                   -> radios(trans('analytics::transaction_log.options.type'))
                   -> label(trans('analytics::transaction_log.label.type'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('avs_code')
                       -> label(trans('analytics::transaction_log.label.avs_code'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.avs_code'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('total_amount')
                       -> label(trans('analytics::transaction_log.label.total_amount'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.total_amount'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('card')
                       -> label(trans('analytics::transaction_log.label.card'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.card'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('json_data')
                       -> label(trans('analytics::transaction_log.label.json_data'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.json_data'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('cart_id')
                       -> label(trans('analytics::transaction_log.label.cart_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.cart_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('added_item')
                       -> label(trans('analytics::transaction_log.label.added_item'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.added_item'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('analytics::transaction_log.label.restaurant_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('url')
                       -> label(trans('analytics::transaction_log.label.url'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.url'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('source')
                       -> label(trans('analytics::transaction_log.label.source'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.source'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('server_variables')
                       -> label(trans('analytics::transaction_log.label.server_variables'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.server_variables'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('HTTP_REFERER')
                       -> label(trans('analytics::transaction_log.label.HTTP_REFERER'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.HTTP_REFERER'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



