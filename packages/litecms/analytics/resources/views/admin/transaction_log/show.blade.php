    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('analytics::transaction_log.name') !!}</a></li>
            <div class="box-tools pull-right">
                                @include('analytics::admin.transaction_log.partial.workflow')
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#analytics-transaction_log-entry' data-href='{{guard_url('analytics/transaction_log/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($transaction_log->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#analytics-transaction_log-entry' data-href='{{ guard_url('analytics/transaction_log') }}/{{$transaction_log->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#analytics-transaction_log-entry' data-datatable='#analytics-transaction_log-list' data-href='{{ guard_url('analytics/transaction_log') }}/{{$transaction_log->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('analytics-transaction_log-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('analytics/transaction_log'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('analytics::transaction_log.name') !!}  [{!! $transaction_log->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('analytics::admin.transaction_log.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>