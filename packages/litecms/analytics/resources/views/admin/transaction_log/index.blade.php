<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('analytics::transaction_log.name') !!} <small> {!! trans('app.manage') !!} {!! trans('analytics::transaction_log.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('analytics::transaction_log.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='analytics-transaction_log-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('analytics/transaction_log')!!}">{!! trans('analytics::transaction_log.names') !!}</a></li>
                    <li class="{!!(request('status') == 'archive')?'active':'';!!}"><a href="{!!guard_url('analytics/transaction_log?status=archive')!!}">Archived</a></li>
                    <li class="{!!(request('status') == 'deleted')?'active':'';!!}"><a href="{!!guard_url('analytics/transaction_log?status=deleted')!!}">Trashed</a></li>
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('analytics/transaction_log/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('analytics::admin.transaction_log.partial.actions')
                    -->
                    @include('analytics::admin.transaction_log.partial.filter')
                    @include('analytics::admin.transaction_log.partial.column')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="analytics-transaction_log-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="analytics-transaction_log-check-all"></th>
                        <th data-field="user_id">{!! trans('analytics::transaction_log.label.user_id')!!}</th>
                    <th data-field="ip_address">{!! trans('analytics::transaction_log.label.ip_address')!!}</th>
                    <th data-field="date">{!! trans('analytics::transaction_log.label.date')!!}</th>
                    <th data-field="type">{!! trans('analytics::transaction_log.label.type')!!}</th>
                    <th data-field="avs_code">{!! trans('analytics::transaction_log.label.avs_code')!!}</th>
                    <th data-field="total_amount">{!! trans('analytics::transaction_log.label.total_amount')!!}</th>
                    <th data-field="card">{!! trans('analytics::transaction_log.label.card')!!}</th>
                    <th data-field="json_data">{!! trans('analytics::transaction_log.label.json_data')!!}</th>
                    <th data-field="cart_id">{!! trans('analytics::transaction_log.label.cart_id')!!}</th>
                    <th data-field="added_item">{!! trans('analytics::transaction_log.label.added_item')!!}</th>
                    <th data-field="restaurant_id">{!! trans('analytics::transaction_log.label.restaurant_id')!!}</th>
                    <th data-field="url">{!! trans('analytics::transaction_log.label.url')!!}</th>
                    <th data-field="source">{!! trans('analytics::transaction_log.label.source')!!}</th>
                    <th data-field="server_variables">{!! trans('analytics::transaction_log.label.server_variables')!!}</th>
                    <th data-field="HTTP_REFERER">{!! trans('analytics::transaction_log.label.HTTP_REFERER')!!}</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    app.load('#analytics-transaction_log-entry', '{!!guard_url('analytics/transaction_log/0')!!}');
    oTable = $('#analytics-transaction_log-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('analytics/transaction_log') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'user_id'},
            {data :'ip_address'},
            {data :'date'},
            {data :'type'},
            {data :'avs_code'},
            {data :'total_amount'},
            {data :'card'},
            {data :'json_data'},
            {data :'cart_id'},
            {data :'added_item'},
            {data :'restaurant_id'},
            {data :'url'},
            {data :'source'},
            {data :'server_variables'},
            {data :'HTTP_REFERER'},
        ],
        "pageLength": 25
    });

    $('#analytics-transaction_log-list tbody').on( 'click', 'tr', function () {
        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#analytics-transaction_log-list').DataTable().row( this ).data();
        $('#analytics-transaction_log-entry').load('{!!guard_url('analytics/transaction_log')!!}' + '/' + d.id);
    });

    $('#analytics-transaction_log-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#analytics-transaction_log-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#analytics-transaction_log-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#analytics-transaction_log-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script>