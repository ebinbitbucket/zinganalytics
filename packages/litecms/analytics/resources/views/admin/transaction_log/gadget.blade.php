@forelse($transaction_log as $key => $val)
<div class="transaction_log-gadget-box">
    <p>{!!@$val->name!!}</p>
    <p class="text-muted"><small><i class="ion ion-android-person"></i> {!!@$val->user->name!!} at {!! format_date($val->created_at)!!}</small></p>
</div>
@empty
<div class="transaction_log-gadget-box">
    <p>No TransactionLog</p>
</div>
@endif