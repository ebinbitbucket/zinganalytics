            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('user_id')
                       -> label(trans('analytics::transaction_log.label.user_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.user_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('ip_address')
                       -> label(trans('analytics::transaction_log.label.ip_address'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.ip_address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='date' class='control-label'>{!!trans('analytics::transaction_log.label.date')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('date')
                            -> placeholder(trans('analytics::transaction_log.placeholder.date'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('type')
                   -> radios(trans('analytics::transaction_log.options.type'))
                   -> label(trans('analytics::transaction_log.label.type'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('avs_code')
                       -> label(trans('analytics::transaction_log.label.avs_code'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.avs_code'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('total_amount')
                       -> label(trans('analytics::transaction_log.label.total_amount'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.total_amount'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('card')
                       -> label(trans('analytics::transaction_log.label.card'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.card'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('json_data')
                       -> label(trans('analytics::transaction_log.label.json_data'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.json_data'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('cart_id')
                       -> label(trans('analytics::transaction_log.label.cart_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.cart_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('added_item')
                       -> label(trans('analytics::transaction_log.label.added_item'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.added_item'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('analytics::transaction_log.label.restaurant_id'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('url')
                       -> label(trans('analytics::transaction_log.label.url'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.url'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('source')
                       -> label(trans('analytics::transaction_log.label.source'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.source'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('server_variables')
                       -> label(trans('analytics::transaction_log.label.server_variables'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.server_variables'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('HTTP_REFERER')
                       -> label(trans('analytics::transaction_log.label.HTTP_REFERER'))
                       -> placeholder(trans('analytics::transaction_log.placeholder.HTTP_REFERER'))!!}
                </div>
            </div>