<div class="btn-group analytics-transaction_log">
    <button class="btn btn-xs btn-danger btn-search" type="button">
        <i aria-hidden="true" class="fa fa-search">
        </i>
        <span class="hidden-sm hidden-xs"> Search</span>
    </button>
    <button aria-expanded="false" class="btn btn-xs btn-danger dropdown-toggle" data-toggle="dropdown" type="button">
        <span class="caret">
        </span>
        <span class="sr-only">
            Toggle Dropdown
        </span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a class="btn-search" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-filter">
                </i>
                Show filters
            </a>
        </li>
        <li>
            <a class="btn-reset-filter" style="cursor:pointer;">
                <i class="fa fa-fw fa-ban text-danger">
                </i>
                Clear filters
            </a>
        </li>
        <li class="divider">
        </li>
        <li>
            <a class="btn-save" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-floppy-o">
                </i>
                Save search
            </a>
        </li>
        <li>
            <a class="btn-open" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-folder-open-o">
                </i>
                Saved searches
            </a>
        </li>
    </ul>
</div>

<div class="modal fade" id="modal-search">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #dd4b39; color: #fff;">
              <button type="button" class="close" data-dismiss="modal" aaria-hidden="true">&times;</button>
              <h4 class="modal-title">Search</h4>
            </div>
              {!!Form::horizontal_open()
              ->id('form-search')
              ->method('POST')
              ->action(guard_url('settings/settings'))!!}
                <div class="modal-body has-form clearfix">
                    <div class="modal-form">
<div class="container-fluid">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[user_id]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.user_id')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[user_id]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[ip_address]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.ip_address')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[ip_address]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[date]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.date')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[date]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[type]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.type')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[type]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[avs_code]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.avs_code')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[avs_code]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[total_amount]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.total_amount')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[total_amount]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[card]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.card')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[card]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[json_data]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.json_data')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[json_data]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[cart_id]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.cart_id')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[cart_id]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[added_item]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.added_item')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[added_item]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[restaurant_id]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.restaurant_id')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[restaurant_id]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[url]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.url')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[url]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[source]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.source')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[source]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[server_variables]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.server_variables')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[server_variables]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[HTTP_REFERER]" class="col-sm-2 control-label">
                                        {!! trans('analytics::transaction_log.label.HTTP_REFERER')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[HTTP_REFERER]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 col-lg-12">
                        <button aria-label="Close" class="btn pull-right btn-danger" data-dismiss="modal" type="button">
                            <i class="fa fa-times-circle">
                            </i>
                            Close
                        </button>
                        <button class="btn btn-success pull-right " id="btn-apply-search" name="new" style="margin-right:1%" type="button">
                            <i class="fa fa-check-circle">
                            </i>
                            Search
                        </button>
                    </div>
                </div>
              {!!Form::close()!!}
        </div>
    </div>
</div>


<div class="modal fade" id="modal-open">
  <div class="modal-dialog">
    <div class="modal-content" style="max-width:400px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Saved</h4>
      </div>
      <div class="modal-body" style="height:210px; overflow-y: auto;">
        
        <div id="saved-list">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger"  name="Closerep" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(document).ready(function(){

    $(".analytics-transaction_log .btn-open").click(function(){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        $('#open-list').load("{!!guard_url('/settings/setting/search/analytics.transaction_log.search')!!}");
        $('#modal-open').modal("show");
    });

   $(".analytics-transaction_log .btn-search").click(function(){
      $('#modal-search').modal("show");
    });
   
    $('.analytics-transaction_log .btn-save').click(function(e){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        var search = prompt("Please enter name for your search");
        if (search == null) {
            toastr.error('Please enter valid name.', 'Error');
            return false;
        }
        var formData = new FormData();
        formData.append('value', $("#form-search").serialize());
        formData.append('name', search);
        formData.append('key', 'analytics.transaction_log.search');
        formData.append('package', 'Page');
        formData.append('module', 'Page');

        $.ajax({
            url : "{!!guard_url('/settings/setting')!!}",
            type: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR)
            {
                toastr.success('Search saved successfully.', 'Success');
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                toastr.error('An error occurred while saving.', 'Error');
            }
        });

        e.preventDefault();
    });

    $('#btn-apply-search').click( function() {
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#analytics-transaction_log-list .btn-reset-filter').css('display', '');
        $('#modal-search').modal("hide");
        
      });
    
    $(".btn-reset-filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#analytics-transaction_log-list .btn-reset-filter').css('display', 'none');

    });

});
</script>