<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">  {!! trans('analytics::transaction_log.names') !!} [{!! trans('analytics::transaction_log.text.preview') !!}]</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-primary btn-sm"  data-action='NEW' data-load-to='#analytics-transaction_log-entry' data-href='{!!guard_url('analytics/transaction_log/create')!!}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }} </button>
        </div>
    </div>
</div>