    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#transaction_log" data-toggle="tab">{!! trans('analytics::transaction_log.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#analytics-transaction_log-edit'  data-load-to='#analytics-transaction_log-entry' data-datatable='#analytics-transaction_log-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#analytics-transaction_log-entry' data-href='{{guard_url('analytics/transaction_log')}}/{{$transaction_log->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('analytics-transaction_log-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('analytics/transaction_log/'. $transaction_log->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="transaction_log">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('analytics::transaction_log.name') !!} [{!!$transaction_log->name!!}] </div>
                @include('analytics::admin.transaction_log.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>