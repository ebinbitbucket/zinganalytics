<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for transaction_log in analytics package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  transaction_log module in analytics package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'TransactionLog',
    'names'         => 'TransactionLogs',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'TransactionLogs',
        'sub'   => 'TransactionLogs',
        'list'  => 'List of transaction_logs',
        'edit'  => 'Edit transaction_log',
        'create'    => 'Create new transaction_log'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'type'                => ['card','cart','restaurant','eateryview','login','checkout','payment'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'user_id'                    => 'Please enter user id',
        'ip_address'                 => 'Please enter ip address',
        'date'                       => 'Please select date',
        'type'                       => 'Please select type',
        'avs_code'                   => 'Please enter avs code',
        'total_amount'               => 'Please enter total amount',
        'card'                       => 'Please enter card',
        'json_data'                  => 'Please enter json data',
        'cart_id'                    => 'Please enter cart id',
        'added_item'                 => 'Please enter added item',
        'restaurant_id'              => 'Please enter restaurant id',
        'url'                        => 'Please enter url',
        'source'                     => 'Please enter source',
        'server_variables'           => 'Please enter server variables',
        'HTTP_REFERER'               => 'Please enter HTTP REFERER',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'user_id'                    => 'User id',
        'ip_address'                 => 'Ip address',
        'date'                       => 'Date',
        'type'                       => 'Type',
        'avs_code'                   => 'Avs code',
        'total_amount'               => 'Total amount',
        'card'                       => 'Card',
        'json_data'                  => 'Json data',
        'cart_id'                    => 'Cart id',
        'added_item'                 => 'Added item',
        'restaurant_id'              => 'Restaurant id',
        'url'                        => 'Url',
        'source'                     => 'Source',
        'server_variables'           => 'Server variables',
        'HTTP_REFERER'               => 'HTTP REFERER',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'user_id'                    => ['name' => 'User id', 'data-column' => 1, 'checked'],
        'ip_address'                 => ['name' => 'Ip address', 'data-column' => 2, 'checked'],
        'date'                       => ['name' => 'Date', 'data-column' => 3, 'checked'],
        'type'                       => ['name' => 'Type', 'data-column' => 4, 'checked'],
        'avs_code'                   => ['name' => 'Avs code', 'data-column' => 5, 'checked'],
        'total_amount'               => ['name' => 'Total amount', 'data-column' => 6, 'checked'],
        'card'                       => ['name' => 'Card', 'data-column' => 7, 'checked'],
        'json_data'                  => ['name' => 'Json data', 'data-column' => 8, 'checked'],
        'cart_id'                    => ['name' => 'Cart id', 'data-column' => 9, 'checked'],
        'added_item'                 => ['name' => 'Added item', 'data-column' => 10, 'checked'],
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 11, 'checked'],
        'url'                        => ['name' => 'Url', 'data-column' => 12, 'checked'],
        'source'                     => ['name' => 'Source', 'data-column' => 13, 'checked'],
        'server_variables'           => ['name' => 'Server variables', 'data-column' => 14, 'checked'],
        'HTTP_REFERER'               => ['name' => 'HTTP REFERER', 'data-column' => 15, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'TransactionLogs',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
