<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Analytics package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for analytics module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Analytics',
    'names'         => 'Analytics',
];
